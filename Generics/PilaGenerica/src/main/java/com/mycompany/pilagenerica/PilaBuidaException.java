
package com.mycompany.pilagenerica;

class PilaBuidaException extends RuntimeException{

    public PilaBuidaException(String message) {
        super(message);
    }

    public PilaBuidaException(String message, Throwable cause) {
        super(message, cause);
    }

    
    
}