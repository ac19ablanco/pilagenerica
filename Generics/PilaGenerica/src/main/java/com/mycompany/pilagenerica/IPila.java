package com.mycompany.pilagenerica;

import java.util.List;

public interface IPila<E> {

    public boolean afegir(E e);

    public E obtenir();

    public E treure() throws PilaBuidaException;

    public List<E> getPila();

    public int mida();

    public boolean esBuida();

}
