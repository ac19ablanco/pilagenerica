package com.mycompany.pilagenerica;

import java.util.ArrayList;
import java.util.List;

public class Pila<E> implements IPila<E>{

    private List<E> dades;

    public Pila() {
        dades = new ArrayList();
    }

    public Pila(List<E> dadesInicials) {
        this.dades = dadesInicials;
    }

    @Override
    public boolean afegir(E e) {
        boolean tornar = false;
        if (e != null) {
            dades.add(e);
            tornar = true;
            return tornar;
        } else {
            return tornar;
        }
    }

    @Override
    public E obtenir() {
        E primer = null;
        if (!dades.isEmpty()) {
            primer = dades.get(dades.size()-1);
        }
        return primer;
    }

    @Override
    public E treure() throws PilaBuidaException {
        E primerEsb = null;
        if (!dades.isEmpty()) {
            primerEsb = dades.get(dades.size()-1);
            dades.remove(primerEsb);

        } else {
            throw new PilaBuidaException("Aquesta pila no està buida");
        }
        return null;
    }

    @Override
    public List<E> getPila() {
        return dades;
    }

    @Override
    public int mida() {
        return dades.size();

    }

    @Override
    public boolean esBuida() {
        return (dades.isEmpty())?true:false;
    }

    @Override
    public String toString() {
        return "Pila{" + "dades=" + dades + '}';
    }

}
