package com.mycompany.pilagenerica;

import dades.Llibre;
import java.util.List;
import java.util.Random;

public class PilaTest {
       
    public static void main(String[] args) {
        
        Pila<Llibre> pilas = new Pila();
        
        Llibre llib1 = new Llibre("Titol4", "Autor4", 2004);
        //afegim 5 llibres
        pilas.afegir(new Llibre("Titol1", "Autor1", 2000));
        pilas.afegir(new Llibre("Titol2", "Autor2", 2002));
        pilas.afegir(new Llibre("Titol3", "Autor3", 2003));
        pilas.afegir(llib1);
//        pilas.afegir(llib1);
        
        System.out.println("Pila llibres: " + pilas);
        
        System.out.println("Element a dalt pila: " + pilas.obtenir());
        
        System.out.println("Treure element a dalt pila: " + pilas.treure());
        
        System.out.println("Mida pila " + pilas.mida());
        
        System.out.println("Pila llibres: " + pilas);
        
        System.out.println("Es repeteix: " + ambRepeticions(pilas));
        
        
        
       
    }
    public static boolean ambRepeticions(Pila<Llibre> pila){
        boolean repeteix = false;
        List repetit = pila.getPila();
        
        if(!pila.esBuida()){
        for(int i = 0; i < pila.mida(); i++){
            for(int j = i+1; j < pila.mida(); j++){
                if(repetit.get(i).equals(repetit.get(j))){
                    repeteix = true;
                    break;
                }
            
            }
            
        }
        
        }
        
    return repeteix;
    }
}