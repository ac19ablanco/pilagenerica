package dades;

public class Llibre implements Comparable<Llibre> {

    private String titol;
    private String autor;
    private int anyEdicio;

    public Llibre(String titol, String autor, int anyEdicio) {
        this.titol = titol;
        this.autor = autor;
        this.anyEdicio = anyEdicio;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getAnyEdicio() {
        return anyEdicio;
    }

    public void setAnyEdicio(int anyEdicio) {
        this.anyEdicio = anyEdicio;
    }

    @Override
    public String toString() {
        return "Llibre{" + "titol=" + titol + ", autor=" + autor + ", anyEdicio=" + anyEdicio + '}';
    }

    @Override
    public int compareTo(Llibre t) {
        return titol.compareTo(t.titol);
    }

}
